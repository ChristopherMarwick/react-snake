import React from 'react';
import ReactDOM from 'react-dom';
import Game from './components/game';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducers/index';
import { Direction } from './util';

var store = createStore(reducer);

ReactDOM.render(
    <Provider store={store}>
        <Game/>
    </Provider>, document.getElementById('root')
);

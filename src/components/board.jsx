import React, { Component } from 'react';
// import Snake from './snake';
import { Direction } from '../util';
import $ from 'jquery';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeDirection, move, eatApple, addApple } from '../actions/index';


class Board extends Component {
    static defaultProps = {
        width: '600',
        height: '600',
        sizeRow: 20
    }

    constructor(props) {
        super(props);
        this.state = {
            gameLoopEnabled: false,
            tickTime: 200
        }
    }

    componentDidMount = () => {
        this.initKeyEvents();
        this.initGameLoop();
        this.props.addApple(this.props.sizeRow, this.props.sizeRow);
    }

    componentWillUnmount = () => {
        this.stopGameLoop();
    }

    componentDidUpdate = () => {
        this.clearBoard();
        // this.addGrid();
        this.addSnake();
        this.addApple();
    }

    initKeyEvents = () => {
        $(document).keydown((e) => {
            var newDirection = this.state.direction;
            var directionMap = {
                'ArrowUp': Direction.UP,
                'ArrowDown': Direction.DOWN,
                'ArrowLeft': Direction.LEFT,
                'ArrowRight': Direction.RIGHT
            };
            if (e.key in directionMap) {
                this.props.changeDirection(directionMap[e.key]);
            }
        });
    }

    checkCollision = () => {
        var head = this.props.snake[this.props.snake.length-1];

        // Wall collision
        if (head[0] < 0 || head[0] >= this.props.sizeRow ||
            head[1] < 0 || head[1] >= this.props.sizeRow)
        {
            this.loseGame();
        }

        // Collision with apple
        var applePos = this.props.applePos;
        if (head[0] === this.props.applePos[0] && head[1] === this.props.applePos[1]) {
            this.props.eatApple();
            this.props.addApple(this.props.sizeRow, this.props.sizeRow);
            this.setState(prevState => {
                return {tickTime: Math.max(prevState.tickTime - 10, 60)};
            })
        }

        // Collision of head with self (that is any non-head part of the
        // snake)
        for (var i=0; i<this.props.snake.length-1; i++) {
            if (head[0] === this.props.snake[i][0] && head[1] === this.props.snake[i][1]) {
                this.loseGame();
            }
        }
    }

    gameTick = () => {
        this.props.move();
        // Collision
        this.checkCollision();
        if (this.state.gameLoopEnabled) {
            setTimeout(this.gameTick, this.state.tickTime);
        }
    }

    initGameLoop = () => {
        setTimeout(this.gameTick.bind(this), this.state.tickTime);
        this.setState((prevState, props) => {return {gameLoopEnabled: true}});
    }

    stopGameLoop = () => {
        this.setState((prevState, props) => {
            return {
                gameLoopEnabled: false
            };
        });
    }

    loseGame = () => {
        // TODO- do more stuff here than simply stop the game, maybe
        // add a lose banner?
        this.stopGameLoop();
    }

    clearBoard = () => {
        var board = document.getElementById('board');
        var ctxt = board.getContext('2d');
        ctxt.clearRect(0, 0, this.props.width, this.props.width);
    }

    addGrid = () => {
        var board = document.getElementById('board');
        var ctxt = board.getContext('2d');
        var x = 0;
        var y = 0;
        var cellSize = this.props.width / this.props.sizeRow;
        for (x=0; x < this.props.sizeRow; x++) {
            for (y=0; y < this.props.sizeRow; y++) {
                ctxt.rect(x*cellSize, y*cellSize, cellSize, cellSize);
            }
        }
        ctxt.stroke();
    }

    addSnake = () => {
        var board = document.getElementById('board');
        var ctxt = board.getContext('2d');
        var cellSize = this.props.width / this.props.sizeRow;
        var point;
        for (var i=0; i<this.props.snake.length; i++) {
            point = this.props.snake[i];
            var centreX = point[0]*cellSize + cellSize/2;
            var centreY = point[1]*cellSize + cellSize/2;
            ctxt.beginPath();
            ctxt.arc(centreX, centreY, cellSize/2, 0, 2 * Math.PI);
            ctxt.fillStyle = 'green';
            ctxt.fill();
            ctxt.stroke();
        }
    }

    addApple = () => {
        var board = document.getElementById('board');
        var ctxt = board.getContext('2d');
        var cellSize = this.props.width / this.props.sizeRow;
        ctxt.fillStyle = 'gold';
        var centreX = (this.props.applePos[0] * cellSize) + (cellSize / 2);
        var centreY = (this.props.applePos[1] * cellSize) + (cellSize / 2);
        ctxt.beginPath();
        ctxt.arc(centreX, centreY, cellSize / 2, 0, 2 * Math.PI);
        ctxt.fill();
        ctxt.stroke();
    }

    render = () => {
        return <div id='div_board'>
            <canvas
                id='board' width={this.props.width} height={this.props.height} style={{border:'1px solid'}}>
            </canvas>
        </div>
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({changeDirection, move, eatApple, addApple}, dispatch);
}

function mapStateToProps(state) {
    return {
        nextDirection: state.nextDirection,
        direction: state.direction,
        snake: state.snake,
        pendingTail: state.pendingTail,
        applePos: state.applePos
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Board);

import { CHANGE_DIRECTION, ADD_APPLE, MOVE, EAT_APPLE } from '../constants/actions';
import { Direction } from '../util';

var initialState = {
    nextDirection: null,
    direction: Direction.RIGHT,
    snake: [[1, 2], [2, 2], [3, 2], [4, 2]],
    pendingTail: [0, 2],
    applePos: null
}

const reducer = (state=initialState, action) => {
    var newState = Object.assign({}, state);
    newState.snake = [...state.snake];
    switch (action.type) {
        case CHANGE_DIRECTION:
            var targetDirection = action.payload.direction;
            var currentDirection = state.direction;
            switch (targetDirection) {
                case Direction.UP:
                    if ([Direction.LEFT, Direction.RIGHT].includes(currentDirection)) {
                        newState.nextDirection = Direction.UP;
                    }
                    break;
                case Direction.DOWN:
                    if ([Direction.LEFT, Direction.RIGHT].includes(currentDirection)) {
                        newState.nextDirection = Direction.DOWN;
                    }
                    break;
                case Direction.LEFT:
                    if ([Direction.UP, Direction.DOWN].includes(currentDirection)) {
                        newState.nextDirection = Direction.LEFT;
                    }
                    break;
                case Direction.RIGHT:
                    if ([Direction.UP, Direction.DOWN].includes(currentDirection)) {
                        newState.nextDirection = Direction.RIGHT;
                    }
                    break;
            }
            break;
        case MOVE:
            var direction = state.direction;
            var head = state.snake[state.snake.length-1];
            if (state.nextDirection) {
                direction = state.nextDirection;
            }
            switch (direction) {
                case Direction.UP:
                    newState.snake.push([head[0], head[1]-1]);
                    break;
                case Direction.DOWN:
                    newState.snake.push([head[0], head[1]+1]);
                    break;
                case Direction.LEFT:
                    newState.snake.push([head[0]-1, head[1]]);
                    break;
                case Direction.RIGHT:
                    newState.snake.push([head[0]+1, head[1]]);
                    break;
            }
            newState.direction = direction;
            newState.nextDirection = null;
            newState.pendingTail = newState.snake.shift();
            break;
        case ADD_APPLE:
            // Get random position for apple within a given width/height,
            // exclude anywhere the snake currently is.
            while (true) {
                var x = Math.floor(Math.random() * action.payload.width);
                var y = Math.floor(Math.random() * action.payload.height);
                for (var i=0; i<state.snake.length; i++) {
                    if (x === state.snake[i][0] && y === state.snake[i][1]) {
                        continue;
                    }
                }
                break;
            }
            newState.applePos = [x, y];
            break;
        case EAT_APPLE:
            newState.snake.unshift(newState.pendingTail);
            break;
    }
    return newState;
}

export default reducer;

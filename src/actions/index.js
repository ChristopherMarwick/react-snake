import { CHANGE_DIRECTION, MOVE, ADD_APPLE, EAT_APPLE } from '../constants/actions';

var changeDirection = (direction) => {
    return {
        type: CHANGE_DIRECTION,
        payload: {
            direction
        }
    }
}

var move = () => {
    return {
        type: MOVE
    }
}

var addApple = (width, height) => {
    return {
        type: ADD_APPLE,
        payload: {
            width,
            height
        }
    }
}

var eatApple = () => {
    return {
        type: EAT_APPLE
    }
}


export { changeDirection, move, addApple, eatApple };
